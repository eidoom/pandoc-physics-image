# [pandoc-physics-image](https://gitlab.com/eidoom/pandoc-physics-image)

Inspired by [pandoc/latex](https://hub.docker.com/r/pandoc/latex) ([source](https://github.com/pandoc/dockerfiles)).
Created after I had some difficulties extending the above.

## TODO

* Optimise size
    * Put latex stuff in its own stage
    * Restore release stage
    * Figure out what of cabal stuff can be deleted after build
