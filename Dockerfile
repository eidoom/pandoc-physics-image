FROM docker.io/library/haskell:8-buster AS builder

RUN apt update && apt install -y \
        wget \
        unzip \
        pdf2svg \
        perl

RUN cabal update && \
        cabal install pandoc && \
        cabal install pandoc-citeproc pandoc-crossref

WORKDIR /usr/local/src/

RUN \
        wget https://github.com/owickstrom/pandoc-include-code/archive/master.zip && \
        unzip master.zip && \
        cd pandoc-include-code-master && \
        cabal configure && \
        cabal install && \
        cd .. && \
        rm -r master.zip pandoc-include-code-master

# FROM docker.io/library/debian:buster-slim

# COPY --from=builder /root/.cabal/bin/* /usr/local/bin
# COPY --from=builder /root/.cabal/store/ghc-8.10.1 /usr/local/store/ghc-8.10.1

# RUN apt update && apt install -y \
#         make \
#         pdf2svg \
#         perl \
        # wget

COPY texlive.profile texlive.profile

RUN \
        wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
        mkdir install-tl && \
        tar --strip-components 1 -xzf install-tl-unx.tar.gz -C install-tl && \
        ./install-tl/install-tl --profile=texlive.profile && \
        rm -rf install-tl install-tl-unx.tar.gz texlive.profile

ENV PATH="/root/.cabal/bin:/opt/texlive/texdir/bin/x86_64-linux/:${PATH}"

RUN tlmgr install \
        logreq \
        framed \
        bidi \
        csquotes \
        lualatex-math \
        xurl \
        tikz-feynhand \
        pgfopts \
        fvextra \
        && \
        rm -rf \
        /opt/texlive/texdir/texmf-dist/doc  \
        /opt/texlive/texdir/readme-html.dir \
        /opt/texlive/texdir/readme-txt.dir  \
        /opt/texlive/texdir/install-tl*
